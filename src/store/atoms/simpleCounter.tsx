import { atom } from 'recoil';

const simpleCounter = atom<number>({
	key: 'simpleCounter',
	default: 0
});

export default simpleCounter;
