import React, { FC } from 'react';

import { useRecoilState } from 'recoil';
import simpleCounter from '~store/atoms/simpleCounter';

import logo from '~res/images/logo.svg';
import '~res/styles/App.scss';

const App: FC = () => {
	const [counter, setCounter] = useRecoilState(simpleCounter);

	return (
		<div className="App">
			<header className="App-header" onClick={() => { setCounter(counter + 1) }}>
				<img src={logo} className="App-logo" alt="logo"/>
				<p>
					Edit <code>src/components/App.tsx</code> and save to reload.
					{(counter === 0)? null: `You clicked logo ${counter} times.`}
				</p>
				<a
					className="App-link"
					href="https://reactjs.org"
					target="_blank"
					rel="noopener noreferrer"
				>
					Learn React
				</a>
			</header>
		</div>
	);
};

export default App;
