const path = require('path');
const ModuleScopePlugin = require("react-dev-utils/ModuleScopePlugin");
const CracoAlias = require("craco-alias");

module.exports = {
  plugins: [
    {
       plugin: CracoAlias,
       options: {
          source: "tsconfig",
          tsConfigPath: "./tsconfig.paths.json",
          baseUrl: "."
       }
    }
  ],
  webpack: {
    configure: webpackConfig => {
      webpackConfig.resolve.plugins.forEach(plugin => {
        if (plugin instanceof ModuleScopePlugin) {
          plugin.allowedPaths.push(path.resolve(__dirname, "res"));
        }
      });
      return webpackConfig;
    }
  }
};